class Unit {
  static dmg = 0;
}

class InfantryUnit extends Unit {
}

class ArcheryUnit extends Unit {
}

class SiegeUnit extends Unit {
}

class CavalryUnit extends Unit {
}

export default {
  'Fighter': class Fighter extends InfantryUnit {
    static dmg = 1;
  },
  'Light infantry': class LightInfantry extends InfantryUnit {
    static dmg = 2;
  },
  'Trained light infantry': class TrainedLightInfantry extends InfantryUnit {
    static dmg = 4;
  },
  'Armored light infantry': class ArmoredLightInfantry extends InfantryUnit {
    static dmg = 6;
  },
  'Trained armored light infantry': class TrainedArmoredLightInfantry extends InfantryUnit {
    static dmg = 12;
  },
  'Infantry': class Infantry extends InfantryUnit {
    static dmg = 8;
  },
  'Trained infantry': class TrainedInfantry extends InfantryUnit {
    static dmg = 16;
  },
  'Heavy infantry': class TrainedInfantry extends InfantryUnit {
    static dmg = 16;
  },
  'Trained heavy infantry': class TrainedHeavyInfantry extends InfantryUnit {
    static dmg = 32;
  },
  'Archers': class Archers extends ArcheryUnit {
    static dmg = 2;
  },
  'Trained archers': class TrainedArchers extends ArcheryUnit {
    static dmg = 4;
  },
  'Armored archers': class ArmoredArchers extends ArcheryUnit {
    static dmg = 4;
  },
  'Trained armored archers': class TrainedArmoredArchers extends ArcheryUnit {
    static dmg = 8;
  },
  'Longbowmen': class Longbowmen extends ArcheryUnit {
    static dmg = 8;
  },
  'Trained longbowmen': class TrainedLongbowmen extends ArcheryUnit {
    static dmg = 16;
  },
  'Catapult': class Catapult extends SiegeUnit {
    static dmg = 10;
  },
  'Trebuchet': class Trebuchet extends SiegeUnit {
    static dmg = 20;
  },
  'Ballista': class Ballista extends SiegeUnit {
    static dmg = 40;
  },
  'Battering ram': class BatteringRam extends SiegeUnit {
    static dmg = 5;
  },
  'Light cavalry': class LightCavalry extends CavalryUnit {
    static dmg = 16;
  },
  'Trained light cavalry': class TrainedLightCavalry extends CavalryUnit {
    static dmg = 32;
  },
  'Heavy cavalry': class HeavyCavalry extends CavalryUnit {
    static dmg = 64;
  },
  'Trained heavy cavalry': class TrainedHeavyCavalry extends CavalryUnit {
    static dmg = 128;
  },
};
