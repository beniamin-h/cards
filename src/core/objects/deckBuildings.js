export default {
  'Empty place': {
    'wood': 'Wooden hut',
    'cut stone': 'Stone wall',
    'dairy': 'Iron mine',
    'meat': 'Fighter',
  },
  'Wooden hut': {
    '$': 'gold',
    'wood': 'Lumberjack',
    'stone': 'Stonemason',
    'meat': 'Huntsman',
    'grain': 'Farm',
    'bows and arrows': 'Archery range',
    'hide': 'Tannery',
    'iron': 'Blacksmith',
    'gold': 'Market',
    'trade rights': 'Trade post',
    'alcohol': 'Tavern',
  },
  'Market': {
    'wood': '2 gold',
    'stone': '2 gold',
    'meat': '2 gold',
    'grain': '2 gold',
  },
  'Lumberjack': {
    '$': 'wood',
    'wood': 'Lumber mill',
  },
  'Stonemason': {
    '$': 'stone',
    'stone': 'Stonecutter',
  },
  'Huntsman': {
    '$': 'meat',
    'wood': 'Hunter',
  },
  'Farm': {
    '$': 'dairy',
    'grain': 'Cattle farm',
    'wood': 'Grapeyard',
  },
  'Cattle farm': {
    '$': 'meat',
    'boards': 'Stable',
  },
  'Grapeyard': {
    '$': 'alcohol',
  },
  'Stable': {
    'grain': 'horses',
  },
  'Lumber mill': {
    '$': 'boards',
  },
  'Stonecutter': {
    '$': 'cut stone',
  },
  'Hunter': {
    '$': 'bows and arrows',
    'bows and arrows': 'Trapper',
  },
  'Trapper': {
    '$': 'hide',
  },
  'Tannery': {
    '$': 'leather',
  },
  'Stone wall': {
    'boards': 'Mansion',
    'iron': 'Barracks',
    'cut stone': 'City walls',
    'bows and arrows': 'Archery tower',
    'Archers': 'Keep',
    'Trained archers': 'Stronghold',
    'alcohol': 'Church',
  },
  'Mansion': {
    '$': '2 gold',
    'armors': 'Armory',
    'bows and arrows': 'Fletcher',
    'tools': 'Siege workshop',
    '2 gold': 'Merchants guild',
  },
  'Iron mine': {
    '$': 'iron',
  },
  'Barracks': {
    '$': 'Light infantry',
    'Light infantry': 'Trained light infantry',
    'leather': 'Armored light infantry',
    'Armored light infantry': 'Trained armored light infantry',
    'weapons': 'Infantry',
    'Infantry': 'Trained infantry',
    'Heavy infantry': 'Trained heavy infantry',
    'horses': 'Knight stable',
  },
  'Archery range': {
    '$': 'Archers',
    'Archers': 'Trained archers',
    'leather': 'Armored archers',
    'Armored archers': 'Trained armored archers',
    'long bows': 'Longbowmen',
    'Longbowmen': 'Trained longbowmen',
  },
  'Blacksmith': {
    '$': 'tools',
    'iron': 'Weaponsmith',
    'tools': 'Toolsmith',
  },
  'Weaponsmith': {
    '$': 'weapons',
    'iron': 'Armorer',
  },
  'Armorer': {
    '$': 'armors',
  },
  'Armory': {
    'Infantry': 'Heavy infantry',
  },
  'Fletcher': {
    '$': 'long bows',
  },
  'Siege workshop': {
    'boards': 'Catapult',
    'Catapult': 'Trebuchet',
    'iron': 'Ballista',
    'wood': 'Battering ram',
    'tools': 'Engineer',
  },
  'Merchants guild': {
    '$': 'trade rights',
    'alcohol': 'Merchant',
  },
  'Knight stable': {
    'weapons': 'Light cavalry',
    'Light cavalry': 'Trained light cavalry',
    'Heavy infantry': 'Heavy cavalry',
    'Heavy cavalry': 'Trained heavy cavalry',
  },
  'Church': {
    '2 gold': 'Spy',
    'weapons': 'Assassin',
    'horses': 'Diplomat',
  },
}
