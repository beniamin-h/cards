export default [
  'Trade post', // multiple gold income x2
  'City walls', // player cannot be attacked unless attacker has any siege weapon
  'Archery tower', // archers dmg x2 if defending /cannot stack/
  'Keep', // defender dmg x4 /cannot stack/, can be destroyed only by trebuchet or catapult
  'Stronghold', // hp: 200, can be attacked only by trebuchet or catapult
  'Merchant', // buy one resource to the hand instead of the discard pile
  'Toolsmith', // produce one resource to the hand instead of the discard pile
  'Tavern', // remove one owned card from the game
  'Spy', // can see other player's cards
  'Assassin', // can remove one other player's unit to the discard pile for 2 gold
  'Diplomat', // can buy one other player's unit for 1 alcohol
  'Engineer', // can remove one other player's building to the discard pile for 1 tool
];
