import Vue from 'vue';
import Player from '@/core/objects/player';

export default new Vue({
  name: 'Game',
  data: {
    players: [],
  },
  methods: {
    startGame(playersNum = 2) {
      this._initPlayers(playersNum);
    },
    _initPlayers(playersNum) {
      for (let i = 0; i < playersNum; i++) {
        this.players.push(new Player(i));
      }
    }
  }
});
