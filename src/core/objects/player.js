import deckBuildings from '@/core/objects/deckBuildings';
import resources from '@/core/objects/resources';
import { shuffle } from '@/core/utils/array';

export default class Player {
  constructor(playerNum) {
    this.playerNum = playerNum;
    this.deck = 'Empty place';
    this.hand = [];
    this.discardPile = [];
    this.drawPile = [];
    this.initHand();
    this.initDrawPile();
    this.initDiscardPile();
  }

  initHand() {
    this.hand.push('wood', 'stone', 'gold');
  }

  initDrawPile() {
    this.drawPile.push('wood');
  }

  initDiscardPile() {
    this.discardPile.push('meat', 'grain');
  }

  playCard(card) {
    const repeatGain = !deckBuildings[this.deck][card] && (card === 'gold' || card === '2 gold');
    const nextIsResource = resources.indexOf(deckBuildings[this.deck][card]) > -1;
    const next = repeatGain || nextIsResource
      ? this.deck
      : deckBuildings[this.deck][card];
    if (next) {
      const thereWasMarket = this.deck === 'Market';
      this.deck = next;
      const gain = nextIsResource
        ? deckBuildings[this.deck][card]
        : deckBuildings[next]['$'];
      if (gain) {
        this.discardPile.unshift(gain);
        if (repeatGain && card === '2 gold') {
          this.discardPile.unshift(gain);
        }
      }
      if (thereWasMarket) {
        this.deck = 'Empty place';
      }
    } else {
      this.deck = 'Empty place';
    }
    this.discardPile.unshift(card);
    this.hand.splice(this.hand.indexOf(card), 1);
    this.drawCard();
  }

  drawCard() {
    if (this.drawPile.length === 0) {
      shuffle(this.discardPile);
      while (this.discardPile.length > 0) {
        this.drawPile.push(this.discardPile.pop());
      }
    }
    if (this.drawPile.length > 0) {
      this.hand.push(this.drawPile.shift());
    }
  }
}
